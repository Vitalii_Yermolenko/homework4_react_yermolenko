import React from 'react'
import { useSelector } from 'react-redux';
import { ProductsList } from '../components/ProductsList';
import { productInFavoritesSelector} from '../redux/selectors';
import { PageWrapper, PageTitle } from "../style/StyleComponents.jsx";

export function Favorites() {
    const productInFavorites = useSelector(productInFavoritesSelector);

    return (
        <PageWrapper>
            <PageTitle>FAVORITES</PageTitle>
            <div>
                <ProductsList
                    products={productInFavorites}
                />
            </div>
        </PageWrapper>
    )
}