import { useSelector } from 'react-redux';
import { useOutletContext } from 'react-router-dom'
import { ProductsList } from '../components/ProductsList';
import { productsIsFavoriteSelector } from '../redux/selectors';
import { PageWrapper, PageTitle } from "../style/StyleComponents.jsx";

export function Store() {
    const productsWithFavorites = useSelector(productsIsFavoriteSelector);


    console.log("Render Store");
    return (
        <PageWrapper>
        <PageTitle>Store</PageTitle>
        <ProductsList
            products={productsWithFavorites}
        />
        </PageWrapper>
    )
}