import { Product } from '../components/Product';
import { CardsWrapper, PageWrapper, PageTitle, BasketCardBlock } from "../style/StyleComponents.jsx";
import { Button } from '../components/Button';
import { useSelector, useDispatch } from 'react-redux';
import { basketProductsSelector, productsIsFavoriteSelector, formVisibleSelector} from '../redux/selectors';
import { openDeleteModalThunk } from '../redux/modal/openDeleteModalThunk';
import { FormDelilery } from '../components/Form';
import { openFormThunk } from '../redux/form/openFormThunk';

export function Basket() {    
    const productList = useSelector(productsIsFavoriteSelector);
    const productsInBasket = useSelector(basketProductsSelector);
    const formVisible = useSelector(formVisibleSelector)
    const dispatch = useDispatch();
    console.log(formVisible);

    if(productList.length === 0){
        return null;
    }

    return (
        <PageWrapper>
            <PageTitle>BASKET</PageTitle>
            {<CardsWrapper>
                {productsInBasket && productsInBasket.map(({ product, quantity }) => {
                    return (
                    <div key={product.id}>
                        <BasketCardBlock>
                            <p>Qantity: {quantity}</p>
                            <Button
                                className="close"
                                text='X'
                                backgroundColor='grey'
                                handleClick={() => {
                                    dispatch(openDeleteModalThunk(product))
                                }} />
                        </BasketCardBlock>
                        <Product
                            key={product.id}
                            product={product}
                        />
                    </div>)
                })}
            </CardsWrapper>}
            <Button className={`delivery`} backgroundColor='red' text='confirm order' 
                   handleClick={() => {
                    dispatch(openFormThunk());
                }}
            />
            {formVisible && <FormDelilery/>}
        </PageWrapper>
    )
}