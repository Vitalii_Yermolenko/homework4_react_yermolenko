import { OPEN_FORM, CLOSE_FORM,CLOSE_FORM_ONLY } from "../actions"


export function formReducer(state = {currentData:[], formVisible:false}, action){
    switch (action.type){
        case OPEN_FORM:
            return {...state, formVisible:true}
        case CLOSE_FORM:
            return {...state, formVisible:false, currentData: action.payload.currentData}
        case CLOSE_FORM_ONLY:
            return {...state, formVisible:false}
        default:
            return state
    }
}