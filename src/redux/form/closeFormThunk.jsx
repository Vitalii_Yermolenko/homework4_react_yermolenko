

import { CLOSE_FORM, DELETE_PRODUCT_IN_BASKET } from "../actions"

export function closeFormThunk(currentData){
    return function(dispatch, getState){
        const state = getState();
        const {basket } = state;
        currentData = { ...currentData, basket }
        dispatch({type: CLOSE_FORM, payload:{currentData}});
        localStorage.setItem("basket", JSON.stringify({}));
        dispatch({type: DELETE_PRODUCT_IN_BASKET});
    }
}