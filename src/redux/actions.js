export const ADD_PRODUCT_TO_BASKET_ACTION_TYPE = 'basket/addProduct';
export const REMOVE_PRODUCT_TO_BASKET_ACTION_TYPE = 'basket/removeProduct';
export const TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE =  'favorites/toggleProduct';
export const GET_PRODUCTS_REQUEST = 'product/get/request';
export const GET_PRODUCTS_SUCCSES = 'product/get/succses';
export const GET_PRODUCTS_ERROR = 'product/get/error';
export const OPEN_MODAL_AGREE = 'modal/agree/open';
export const CLOSE_MODAL_AGREE = 'modal/agree/close';
export const OPEN_MODAL_DELETE = 'modal/delete/open';
export const CLOSE_MODAL_DELETE = 'modal/delete/close';
export const CHANGE_CHOOSE_PRODUCT = 'product/choose/change';
export const OPEN_FORM = 'form/open';
export const CLOSE_FORM = 'form/close';
export const DELETE_PRODUCT_IN_BASKET = 'basket/deleteProducts';
export const CLOSE_FORM_ONLY = 'form/onlyClose'




