import { TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE } from "../actions"


const savedFavorites = localStorage.getItem("favorites") === null ? [] : JSON.parse(localStorage.getItem("favorites"));


export function favoritesReducer(state = savedFavorites, action){
    switch(action.type){
        case TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE:
            return state.includes(action.payload.productId) ? state.filter(productId => productId !== action.payload.productId) : [...state, action.payload.productId]
        default:
            return state
    }

}