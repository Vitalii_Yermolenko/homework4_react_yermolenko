import { TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE } from "../actions";

export function toogleFavoritesThunk(productId){
  return function(dispatch,getState){
    dispatch({
      type: TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE,
      payload: {productId}
    });

    const state = getState();
    const {favorites } = state;
    localStorage.setItem("favorites", JSON.stringify(favorites));
  }
}