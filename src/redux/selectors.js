

export const favoritesSelector = state => state.favorites; 
export const basketSelector = state => state.basket;

export const productsIsFavoriteSelector = state => {
   return state.products.products.map(product => ({
        ...product,
        isFavorite: state.favorites.includes(product.id)
    }))
}

export const productInFavoritesSelector = state => {
    return productsIsFavoriteSelector(state).filter(product => state.favorites.includes(product.id))
}


export const basketLengthSelector = state => {
    let sum = 0;
    Object.values(state.basket).forEach(value => {
        sum += value;
    });
    return sum;
}

export const modalAgreeSelector = state => state.modal.modalAgreeVisible;
export const modalDeleteSelector = state => state.modal.modalDeleteVisible;

export const basketProductsSelector = state => {
    return Object.entries(state.basket).map(([productId, quantity]) => {
       const product = state.products.products.find(product => product.id === +productId)
       return {
        product,
        quantity
       }
    })
}

export const chooseProductSelector = state => state.products.chooseProduct;
export const formVisibleSelector = state => state.form.formVisible;
