import { REMOVE_PRODUCT_TO_BASKET_ACTION_TYPE, CLOSE_MODAL_DELETE } from "../actions";

export function removeBasketThunk(productId){
    return function(dispatch, getState){
        dispatch({type:REMOVE_PRODUCT_TO_BASKET_ACTION_TYPE, payload:{productId:productId}});
        const state = getState();
        const {basket } = state;
        localStorage.setItem("basket", JSON.stringify(basket));
        dispatch({type: CLOSE_MODAL_DELETE});
    }
}