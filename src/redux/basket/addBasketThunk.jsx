import { ADD_PRODUCT_TO_BASKET_ACTION_TYPE, CLOSE_MODAL_AGREE } from "../actions";

export function addBasketThunk(productId){
    return function(dispatch, getState){
        dispatch({type:ADD_PRODUCT_TO_BASKET_ACTION_TYPE, payload:{productId:productId}});
        const state = getState();
        const {basket } = state;
        localStorage.setItem("basket", JSON.stringify(basket));
        dispatch({type: CLOSE_MODAL_AGREE});
    }
}