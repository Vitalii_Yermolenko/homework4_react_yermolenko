
import { ADD_PRODUCT_TO_BASKET_ACTION_TYPE, REMOVE_PRODUCT_TO_BASKET_ACTION_TYPE,DELETE_PRODUCT_IN_BASKET } from "../actions"

const savedBasket = localStorage.getItem("basket") === null ? {} : JSON.parse(localStorage.getItem("basket"));

export function basketReducer(state = savedBasket, action){
    switch(action.type){
        case ADD_PRODUCT_TO_BASKET_ACTION_TYPE:
            const productId = action.payload.productId;
            return {
                ...state, [productId]: state[productId] ? state[productId] + 1 : 1
            } 

        case REMOVE_PRODUCT_TO_BASKET_ACTION_TYPE:
            const productIdRemove = action.payload.productId;
            const { [productIdRemove]: deletedProduct, ...newBasket } = state;
            return state = newBasket
        case DELETE_PRODUCT_IN_BASKET:
            return {}
        default:
            return state
    }
}

