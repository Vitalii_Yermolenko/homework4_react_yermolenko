import { CHANGE_CHOOSE_PRODUCT, OPEN_MODAL_DELETE } from "../actions"

export function openDeleteModalThunk(product){
    return function(dispatch){
        dispatch({type:CHANGE_CHOOSE_PRODUCT, payload:{chooseProduct:product}})
        dispatch({type: OPEN_MODAL_DELETE})
    }
}