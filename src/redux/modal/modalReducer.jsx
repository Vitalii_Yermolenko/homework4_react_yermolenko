import { OPEN_MODAL_AGREE, CLOSE_MODAL_AGREE, OPEN_MODAL_DELETE ,CLOSE_MODAL_DELETE } from "../actions"

export function modalReducer(state = {modalAgreeVisible:false, modalDeleteVisible:false}, action){
    switch (action.type){
        case OPEN_MODAL_AGREE:
            return {...state, modalAgreeVisible:true}
        case CLOSE_MODAL_AGREE:
            return {...state, modalAgreeVisible:false}
        case OPEN_MODAL_DELETE:
            return {...state, modalDeleteVisible:true}
        case CLOSE_MODAL_DELETE:
             return {...state, modalDeleteVisible:false}
        default:
            return state
    }
}