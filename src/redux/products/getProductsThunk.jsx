import { GET_PRODUCTS_REQUEST,GET_PRODUCTS_SUCCSES,GET_PRODUCTS_ERROR } from "../actions"


export function getProductsThunk(){
    return function(dispatch) {
      dispatch({type:GET_PRODUCTS_REQUEST})
        fetch("./data.json", {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
          })
          .then(response => response.json())
          .then(data => dispatch({type:GET_PRODUCTS_SUCCSES, payload:{products: data}}))
          .catch(error => {
            dispatch({type:GET_PRODUCTS_ERROR})
            console.error(error)
          });
    }
}