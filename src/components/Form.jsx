import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {PatternFormat } from "react-number-format";
import { Wrapper } from "../style/StyleComponents";
import { useDispatch } from "react-redux";
import { closeFormThunk } from "../redux/form/closeFormThunk";
import { WrapperForm,FormButtonCloseWrapper } from "../style/StyleComponents";
import { Button } from "./Button";
import { CLOSE_FORM_ONLY } from "../redux/actions";
import styled from "styled-components";

const FormSchema = Yup.object().shape({
  firstName: Yup.string()
    .matches(/^[a-zA-Zа-яА-ЯЁёІіЇїЄє\s]+$/, "Only letters are allowed")
    .required("Required"),
  lastName: Yup.string()
  .matches(/^[a-zA-Zа-яА-ЯЁёІіЇїЄє\s]+$/, "Only letters are allowed")
  .required("Required"),
  age: Yup.number()
    .typeError("Age must be a number")
    .integer("Age must be a number")
    .max(110, "Enter your valid age")
    .required("Required"),
  address: Yup.string()
  .required("Required"),
  phone: Yup.string()
  .test(
    "phone",
    "Phone number must be in format +3 (###) #### ###",
    (value) => !!value && value.match(/^\+\d \(\d{3}\) \d{4} \d{3}$/)
  )
  .required("Required"),
});

const initialValues = {
  firstName: "",
  lastName: "",
  age: "",
  address: "",
  phone: "",
};


const FormStyled = styled(Form)`
  display:flex;
  flex-direction: column;
  gap: 10px;
  justify-content: center;
  div{
    height: 40px;
    width: 336px;
    input{
      width: 100%;
    }
  }
`



export function FormDelilery(){

  const dispatch = useDispatch();
  return (
    <Wrapper 
      onClick={() => 
        dispatch({type: CLOSE_FORM_ONLY})
      }>
        <WrapperForm
          onClick={(e) => {
          e.stopPropagation();
          }}>
            <FormButtonCloseWrapper>
              <Button 
                className={`form_delivery__close`} 
                text='X' 
                handleClick={(e) =>{
                  dispatch({type: CLOSE_FORM_ONLY})
                }}/>
            </FormButtonCloseWrapper>
          <Formik
            initialValues={initialValues}
            onSubmit={(values, actions) => {
                console.log(JSON.stringify(values, null, 2));
                actions.setSubmitting(false);
              dispatch(closeFormThunk(values));
            }}
            validationSchema={FormSchema}
          >
            {({ errors, touched }) => (
              <FormStyled>
                <div>
                  <label htmlFor="firstName">First Name:</label>
                  <Field name="firstName" />
                  {touched.firstName && errors.firstName ? (
                    <div>{errors.firstName}</div>
                  ) : null}
                </div>
                <div>
                  <label htmlFor="lastName">Last Name:</label>
                  <Field name="lastName" />
                  {touched.lastName && errors.lastName ? (
                    <div>{errors.lastName}</div>
                  ) : null}
                </div>
                <div>
                  <label htmlFor="age">Age:</label>
                  <Field name="age" type="number" />
                  {touched.age && errors.age ? <div>{errors.age}</div> : null}
                </div>
                <div>
                  <label htmlFor="address">Delivery address:</label>
                  <Field name="address" placeholder="country,city,street,house number, apartment number"/>
                  {touched.address && errors.address ? (
                    <div>{errors.address}</div>
                  ) : null}
                </div>
                <div>
                  <label htmlFor="phone">Phone:</label>
                  <Field name="phone">
                    {({ field }) => (
                      <PatternFormat
                        {...field}
                        format="+3 (###) #### ###"
                        allowEmptyFormatting
                        mask="_"
                      />
                    )}
                  </Field>

                  {touched.phone && errors.phone ? <div>{errors.phone}</div> : null}
                </div>
                <button type="submit">Checkout</button>
              </FormStyled>
            )}
          </Formik>
        </WrapperForm>
    </Wrapper>
  );
};
