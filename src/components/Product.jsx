import React from "react";
import { Button } from "./Button"
import { Card, CardText, CardButtons } from "../style/StyleComponents";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux/es/exports";
import { toogleFavoritesThunk } from "../redux/favorites/toogleFavoritesThunk";
import { openAgreeModalThunk } from "../redux/modal/openAgreeModalThunk";

export function Product({product}) {

    const dispatch = useDispatch();
    

    return (
      <Card className="product">
        <h3>{product.name}</h3>
        <img src={product.image} alt="" />
        <CardText>
        <p>Price: {product.price} грн</p>
        <p>Article: {product.article}</p>
        <p style={{
          backgroundColor:product.color,
          borderRadius: '50%',
          width: '20px',
          height: '20px',
          }}></p>
        </CardText>

        <CardButtons>
          <Button 
            text='Buy' 
            className='button-buy'
            handleClick={() => {
              dispatch(openAgreeModalThunk(product))
            }}
            backgroundColor='#3A93FF'
          />
          <svg         
            className='button-favorite'
            onClick={() =>{
              dispatch(toogleFavoritesThunk(product.id))
            } }
            viewBox="0 0 30 30"
            width="40"
            height="40"
            fill={product.isFavorite ? "green" : "none"}
            stroke="currentColor"
            strokeWidth="1"
            strokeLinecap="round"
            strokeLinejoin="round">
            <path d="M12 2 L15.09 8.45 L22 9.55 L17 14.24 L18.18 21.01 L12 17.77 L5.82 21.01 L7 14.24 L2 9.55 L8.91 8.45 Z" />
          </svg>
        </CardButtons>
      </Card>
    );
  }



Product.propTypes = {
  product: PropTypes.object,
}