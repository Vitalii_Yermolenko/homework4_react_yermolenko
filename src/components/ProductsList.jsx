import React from "react";
import { Product } from "./Product";
import PropTypes from "prop-types";
import { CardsWrapper } from "../style/StyleComponents.jsx";
import { useSelector } from "react-redux";
import { productsIsFavoriteSelector } from "../redux/selectors";

export function ProductsList({products }) {

  if(!products){
    return(
      <div>No products</div>
    )
  }

  return (
    <CardsWrapper>
      {products.map((product) => (
        <Product
          key={product.id}
          product={product}
        />
      ))}
      </CardsWrapper>
  );
}

ProductsList.propTypes = {
  products: PropTypes.array,
}
